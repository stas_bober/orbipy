# OrbiPy

OrbiPy contains a decent set of numerical tools for study and research
purpose of an orbital spacecraft dynamics in Circular Restricted Three
Body Problem.

# Core Developer
Stanislav Bober 
	
[MIEM NRU HSE](https://miem.hse.ru/)

[IKI RAS](http://iki.rssi.ru/)

# Developers
Mariia Guskova

[MIEM NRU HSE](https://miem.hse.ru/)
