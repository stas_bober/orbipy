# -*- coding: utf-8 -*-
"""
Created on Mon Jan 21 09:32:57 2019

@author: stasb

Multiprocessed calculation of 10200 orbits with spacecraft initial positions
placed at XZ plane within [-1 mln km, 0]x[0, 1 mln km] square. Right-bottom
corner of square is L2 point and Sun-Earth system is considered.

For debug_run:
    Use Spyder or other Python IDE for testing and debugging.

For execution there are two ways:
    - In system (or anaconda) console: python xz_map_mp.py
    - In Spyder: Run -> Configuration per file... [Crtl+F6]
        1. Console -> Execute in external system terminal
        2. [Optionally] External system terminal -> Interact with the Python console after execution
        3. Run
        
Calculation process can be terminated and resumed later.
All done jobs remains done.
"""

import orbipy as op
from orbipy import mp
import numpy as np
import os
from itertools import product

#%%

# Calculate orbit for 100 revolutions and return spacecraft states in DataFrame
def do_calc(job, folder):
    model = op.crtbp3_model()
    stmmodel = op.crtbp3_model(stm=True)
    point = model.L2
    
    dl = np.abs((1-model.mu)-point)
    
    left  = op.eventX(point-dl+10000/model.R, accurate=False)
    right = op.eventX(point+dl*0.75, accurate=False)
    
    first_corr = op.border_correction(model,
                                      op.y_direction(),
                                      [left], [right],
                                      dv0=0.1, maxt=10000)
    
    corr = op.border_correction(model,
                                op.unstable_direction_stm(stmmodel, event=np.pi, ignore_z=True),
                                [left], [right],
                                dv0=1e-12, maxt=10000)
    
    sk = op.simple_station_keeping(model, 
                                   first_corr, 
                                   corr,
                                   rev=np.pi)
    mp.mprint()
    s0 = model.get_zero_state()
    s0[0] = point + job[0] / model.R
    s0[2] = job[1] / model.R
    df = sk.prop(0.0, s0, N=100)
    return df

# Save orbit DataFrame to pickle (binary) format;
# filename generated using initial x and z coordinates
def do_save(item, folder):
    job = item['job']
    filename = 'orbit_[x]%010.1f_[z]%010.1f'%job
    mp.mprint(filename)
    item['res'].to_pickle(os.path.join(folder, filename+'.pkl'))


#%%

if __name__ == '__main__':       
    folder = 'xz_map'
    
    jobs_todo = product(np.linspace(-1e6, 0., 101),
                        np.linspace(0., 1e6, 101))
    
    m = mp('map_1m', do_calc, do_save, folder).update_todo_jobs(jobs_todo)

#   test and debug do_calc and do_save functions
#    m.debug_run()

#   run multiprocessed calculation only if debug_run runs without errors  
    m.run(p=4)
    