# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 23:24:00 2018

@author: stasb

Comparison of direction calculation algorithms using montecarlo station keeping
with uncertainties:
    by position: 10 km
    by velocity: 15 cm/s
    by delta-v: 5 percent
Random seed and number of corrections are equal for all algorithms.

Better algorithm produce less total delta-v.

"""

import numpy as np
import numpy.random as npr
#import pandas as pd
import matplotlib.pyplot as plt
import orbipy as op

model = op.crtbp3_model(stm=False)
plotter = op.plotter.from_model(model, length_units='Mm', velocity_units='m/s')#
scale = plotter.scaler
stm_model = op.crtbp3_model(stm=True)

s0 = model.get_zero_state()    
x0km = -500000
z0km = 500000

s0[0] = model.L2 + x0km/model.R   
s0[2] = z0km/model.R


left   = op.eventX(model.L2-1400000/model.R)
right  = op.eventX(model.L2+1400000/model.R)

T = scale(4800, 'd-nd')
dt = scale(80, 'd-nd')

first_corr = op.border_correction(model, op.y_direction(), [left], [right])

dirs = (('x', '-b', op.x_direction()),
        ('"true"', '-r', op.unstable_direction(model, disturbance=1e-5, maxt=dt)),
        ('inv(stm)[0:3,0:3]', '-xg', op.unstable_direction_stm(stm_model, maxt=dt, alg='inv')),
        ('inv(stm)[0:3,0:3] xy', '-+g', op.unstable_direction_stm(stm_model, ignore_z=True, event=dt, alg='inv')),
        ('stm[0:3,3:6].T@stm[0:3,3:6]', '-xm', op.unstable_direction_stm(stm_model, event=dt)),
        ('stm[0:3,3:6].T@stm[0:3,3:6] xy', '-+m', op.unstable_direction_stm(stm_model, event=dt, ignore_z=True)),
        )

_, ax = plt.subplots(1,1,figsize=(14,7))

for dname, style, direction in dirs:
    corr_planes = op.border_correction(model, direction, [left], [right])
    
    mcsk = op.montecarlo_station_keeping(model,
                                         first_corr,
                                         corr_planes,
                                         unc_pos=(10, 'km'),
                                         unc_vel=(15, 'cm/s'),
                                         unc_dv=5,
                                         rev=dt)

    npr.seed(11) # seed defines random sequence
    df = mcsk.prop(0.0, s0, N=int(T/dt))

    plotter.plot_proj(df, fsize=(4,4), centers={'x':model.L2},
                      colors='rgb', linewidth=0.5)

    dvarr = np.vstack(tuple(mcsk.dvout))
    dvs = np.sqrt(np.sum(dvarr[1:,4:7]**2, axis=1))
    ax.plot(scale(dvarr[1:,0],'nd-d'), scale(dvs,'nd/nd-m/s'), style, label=dname)
    ax.set_xlabel('Time, days')
    ax.set_ylabel('$\Delta$V, m/s')
    print(dname+' total_dv:', scale(dvs.sum(),'nd/nd-m/s'))
ax.legend()
plt.show()